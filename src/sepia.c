#include "sepia.h"

static uint8_t saturation_value(uint64_t x) {
    return x < 256? x : 255;
}

static void process_sepia_on_pixel(struct pixel* const pixel) {

    static const float c[3][3] = {
            {.393f, .769f, .189f},
            {.349f, .686f, .168f},
            {.272f, .534f, .131f}
    };
    const struct pixel old = *pixel;

    pixel->r =  saturation_value(c[0][0] * old.r + c[0][1] * old.g + c[0][2] * old.b);
    pixel->g =  saturation_value(c[1][0] * old.r + c[1][1] * old.g + c[1][2] * old.b);
    pixel->b =  saturation_value(c[2][0] * old.r + c[2][1] * old.g + c[2][2] * old.b);
}

struct image process_sepia_c(const struct image img) {
    struct create_result res = create_image(img.width, img.height);
    if (res.status == CREATED) {
        struct image sepia_img = res.img;
        for (size_t y = 0; y < img.height; y++) {
            for (size_t x = 0; x < img.width; x++) {
                size_t pixel_index = (size_t) y * img.width + x;
                struct pixel p = get_pixel(img, pixel_index);
                process_sepia_on_pixel(&p);
                set_pixel(&sepia_img, pixel_index, p);
            }
        }
        return sepia_img;
    }
    return img;
}

#define num_of_processed_pixels 4
extern void process_sepia_on_pixels_asm(struct pixel[static num_of_processed_pixels], struct pixel* result);

struct image process_sepia_asm(const struct image img) {
    struct create_result res = create_image(img.width, img.height);
    if (res.status == CREATED) {
        struct image sepia_img = res.img;
        size_t img_size = img.height * img.width;
        size_t remainder = img_size % num_of_processed_pixels;

        for (size_t i = 0; i < img_size; i += 4) {
            process_sepia_on_pixels_asm(img.data + i, sepia_img.data + i);
        }

        for (size_t j = 0; j < remainder; j++) {
            size_t start = img_size - remainder;
            struct pixel p = get_pixel(img, start + j );
            process_sepia_on_pixel(&p);
            set_pixel(&sepia_img, start + j, p);
        }
        return sepia_img;
    }
    return img;
}