#include <stdio.h>

void println_err(const char* err) {
    fprintf(stderr, "%s\n", err);
}