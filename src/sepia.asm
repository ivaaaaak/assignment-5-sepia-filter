%define cycles 3
%define shuffle_coefficients 0b01111001

section .rodata:
    align 16
    red:   dd 0.272, 0.349, 0.393, 0.272
    green: dd 0.534, 0.686, 0.769, 0.534
    blue:  dd 0.131, 0.168, 0.189, 0.131
    max: dd 0xFF, 0xFF, 0xFF, 0xFF

section .text

%macro load_coefficients 0
    movdqa xmm3, [blue]
    movdqa xmm4, [green]
    movdqa xmm5, [red]
%endmacro

%macro shift_coefficients 0
    shufps xmm3, xmm3, shuffle_coefficients
    shufps xmm4, xmm4, shuffle_coefficients
    shufps xmm5, xmm5, shuffle_coefficients
%endmacro

%macro load 1

    %if %1 = 0
        %define pattern 0b01000000
    %elif %1 = 1
        %define pattern 0b01010000
    %elif %1 = 2
        %define pattern 0b01010100
    %endif

    pxor xmm0, xmm0
    pxor xmm1, xmm1
    pxor xmm2, xmm2


    pinsrb xmm0, [rdi + %1 * 3], 0
    pinsrb xmm1, [rdi + %1 * 3 + 1], 0
    pinsrb xmm2, [rdi + %1 * 3 + 2], 0

    pinsrb xmm0, [rdi + %1 * 3 + 3], 4
    pinsrb xmm1, [rdi + %1 * 3 + 4], 4
    pinsrb xmm2, [rdi + %1 * 3 + 5], 4

    cvtdq2ps xmm0, xmm0
    cvtdq2ps xmm1, xmm1
    cvtdq2ps xmm2, xmm2

    shufps xmm0, xmm0, pattern
    shufps xmm1, xmm1, pattern
    shufps xmm2, xmm2, pattern
%endmacro

%macro compute 0
    mulps xmm0, xmm3
    mulps xmm1, xmm4
    mulps xmm2, xmm5

    addps xmm0, xmm1
    addps xmm0, xmm2

    cvtps2dq xmm0, xmm0
    pminud xmm0, [max]
%endmacro

%macro save 1
    pextrb [rsi + %1 * 4], xmm0, 0
    pextrb [rsi + %1 * 4 + 1], xmm0, 4
    pextrb [rsi + %1 * 4 + 2], xmm0, 8
    pextrb [rsi + %1 * 4 + 3], xmm0, 12
%endmacro


global process_sepia_on_pixels_asm
process_sepia_on_pixels_asm:

    load_coefficients
    %assign i 0
    %rep cycles
        load i
        compute
        save i
        shift_coefficients
        %assign i i + 1
    %endrep

    ret