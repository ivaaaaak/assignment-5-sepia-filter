#include "rotation.h"
#include "image.h"
#include <stdlib.h>

struct image rotate(struct image const source) {
    struct create_result res = create_image(source.height, source.width);
    if (res.status == CREATED) {
        struct image rotated_img = res.img;

        for (size_t row = 0; row < rotated_img.height; row++) {
            for (size_t column = 0; column < rotated_img.width; column++) {
                size_t cur_index = (size_t) rotated_img.width * row + column;
                size_t old_index = (size_t) source.width * (source.height - column - 1) + row;
                set_pixel(&rotated_img, cur_index, get_pixel(source, old_index));
            }
        }
        return rotated_img;
    }
    return source;
}
