#include "image.h"

struct create_result create_image(const uint64_t width, const uint64_t height) {
    struct pixel* data = malloc(sizeof(struct pixel) * width * height);
    if (data != NULL) {
        struct create_result res = {
                .status = CREATED,
                .img = { width, height, data }
        };
        return res;
    }
    struct create_result res = { MALLOC_ERROR, {0} };
    return res;
}

void destroy_image(struct image* const img) {
    img->height = 0;
    img->width = 0;
    free(img->data);
}

void set_pixel(const struct image* const img, const size_t pixel_index, const struct pixel new_pixel) {
    img -> data[pixel_index] = new_pixel;
}

struct pixel get_pixel(const struct image img, const size_t pixel_index) {
    return img.data[pixel_index];
}
