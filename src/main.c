#include "image.h"
#include "bmp.h"
#include "sepia.h"
#include "rotation.h"
#include "util.h"
#include <stdio.h>
#include <sys/resource.h>

#define ARGS_NUM 4

long get_current_time() {
    struct rusage r = {0};
    getrusage(RUSAGE_SELF, &r);
    return r.ru_utime.tv_usec;
}

int main(int argc, char** argv) {
    if (argc < ARGS_NUM) {
        println_err("3 arguments are required: <source_file> <target_file> <r|s|S> ");
        return 1;
    }
    FILE* in = fopen(argv[1], "rb");

    if (in != NULL) {
        struct image img = {0};
        enum read_status cur_read_status = from_bmp(in, &img);
        println_err(read_status_string[cur_read_status]);

        if (cur_read_status != READ_OK) {
            if (fclose(in) == 0) {
                println_err("Failed to close source file");
            }
            destroy_image(&img);
            return 1;
        }
        if (fclose(in) == 0) {

            FILE *out = fopen(argv[2], "wb");
            if (out != NULL) {
                struct image res = {0};
                long start = get_current_time();
                switch (argv[3][0]) {
                    case 'r':
                        res = rotate(img);
                        break;
                    case 's':
                        res = process_sepia_c(img);
                        break;
                    case 'S':
                        res = process_sepia_asm(img);
                        break;
                }
                long end = get_current_time();
                printf("Execution time: %ld μs\n", end - start);
                enum write_status cur_write_status = to_bmp(out, &res);
                println_err(write_status_string[cur_write_status]);

                if (cur_write_status != WRITE_OK) {
                    if (fclose(out) == 0) {
                        println_err("Failed to close target file");
                    }
                    destroy_image(&img);
                    destroy_image(&res);
                    return 1;
                }
                destroy_image(&img);
                destroy_image(&res);

                if (fclose(out) != 0) {
                    println_err("Failed to close target file");
                    return 1;
                }
                return 0;

            } else {
                println_err("Failed to open target file");
                destroy_image(&img);
                return 1;
            }

        } else {
            println_err("Failed to close source file");
            destroy_image(&img);
            return 1;
        }

    } else {
        println_err("Failed to open source file");
        return 1;
    }


}
