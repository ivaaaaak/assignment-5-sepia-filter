#ifndef SEPIA_H
#define SEPIA_H
#include "image.h"

struct image process_sepia_c(struct image img);
struct image process_sepia_asm(struct image img);

#endif